#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
longqi 2/Sep/16 15:27
Description:

"""

# Method 1:
# issue: can not handle perfect power of prime number, like 8,25,27

n = 99
i = 2
while i * i < n:
	while n % i == 0:
		n = n / i
	i += 1
print(n)

# Method 2
n = 600851475143
i = 2
while i * i <= n:
	while n % i == 0 and n != i:
		n = n / i
	i += 1

print(n)
