#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
longqi 2/Sep/16 12:08
Description:


"""
# Method 1
result = []
for i in range(1, 1000):
	if i % 3 == 0 or i % 5 == 0:
		result.append(i)
print(sum(result))

# Method 2
result2 = [i for i in range(1, 1000) if i % 3 == 0 or i % 5 == 0]

print(sum(result2))
