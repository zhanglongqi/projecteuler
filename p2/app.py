#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
longqi 2/Sep/16 14:19
Description:


"""

# Method 1
fib_num = [1, 2]
even_num = []
for i in range(2, 1000):
	new_fib = fib_num[i - 1] + fib_num[i - 2]
	fib_num.append(new_fib)
	if new_fib % 2 == 0:
		even_num.append(new_fib)

	if new_fib > 4000000:
		break

print(fib_num)
print(sum(even_num) + 2) # add the second number in

"""
1	2	3
5	8	13
21	34	55
89	144	233

there is one odd number and two even numbers in a three-number group
"""

# Method 2
fib_num = [1, 2]
even_num = []
for i in range(2, 1000):
	new_fib = fib_num[i - 1] + fib_num[i - 2]
	fib_num.append(new_fib)
	if i % 3 == 1:
		even_num.append(fib_num[i])

	if new_fib > 4000000:
		break

print(fib_num)
print(sum(even_num) + 2)
